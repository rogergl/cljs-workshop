(ns workshop.core
  "adding remote debuging
  1. start repl
  2. load page
  3.(require 'weasel.repl.websocket)
  4. (cemerick.piggieback/cljs-repl :repl-env (weasel.repl.websocket/repl-env))"
  (:require [om.core :as om :include-macros true]
            [weasel.repl :as repl]
            [om.dom :as dom :include-macros true]))

(if-not (repl/alive?)
  (repl/connect "ws://localhost:9001"))

(enable-console-print!)

(def app-state (atom {:text "Hello world!"
                      :sub {:text "Hello World"}}))

(defn component [app owner]

  (reify om/IRender
    (render [_]
      (dom/h1 nil (:text app)
              (dom/button #js{:onClick (fn [el]
                                         (om/update! app :text (str (:text app) "Hello"))
                                         (println app-state))} "click Me")))))
(om/root
  (fn [app owner]
    (reify om/IRender
      (render [_]
        (om/build component (:sub app)))))
  app-state
  {:target (. js/document (getElementById "app"))})
