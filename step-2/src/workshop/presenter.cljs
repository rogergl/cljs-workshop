(ns workshop.presenter
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))


(defn component [app owner]
  (reify om/IRender
    (render [_]
      (dom/h1 nil (:text app)))))
