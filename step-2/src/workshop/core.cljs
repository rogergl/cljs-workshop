(ns workshop.core
  (:require [om.core :as om :include-macros true]
            [workshop.presenter :as presenter]))

(enable-console-print!)

(def app-state (atom {:text "Hello world!"}))

(om/root
  (fn [app owner]
    (reify om/IRender
      (render [_]
        (om/build presenter/component app))))
  app-state
  {:target (. js/document (getElementById "app"))})
