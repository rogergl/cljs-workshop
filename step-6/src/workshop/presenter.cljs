(ns workshop.presenter
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(defn- font-style
  ([font-size] (font-style font-size "#000000"))
  ([font-size color]
    #js{:style #js{:fontSize (str font-size "em")
                   :fontFamily "Arial"
                   :lineHeight "1.5em"
                   :color color}}))

(defmulti render-page (fn [page] (:type page)))


(defmethod render-page :title [page]
  (dom/div #js{:className "centered"}
           (dom/span (font-style 6) (:content page))))

(defmethod render-page :section [page]
  (dom/div #js{:className "centered"}
           (dom/span (font-style 5 "3388ff") (:content page))))

(defmethod render-page :list [page]
  (dom/div (font-style 4) (:title page)
           (apply dom/ul {}
                  (map (fn [e] (dom/li (font-style 1) e)) (:content page)))))

(defn component [app _]
  (reify om/IRender
    (render [_]
      (dom/div #js{:style #js{:padding "3em"}}
               (let [page (nth (:pages app) (:counter app))]
                 (render-page page))))))
