(ns cljsinit.core-test
  (:require
    [cljs.test :refer-macros [deftest testing is use-fixtures]]
    [workshop.pages :as pages]))

(enable-console-print!)

(deftest pages
         (let [fixture-1 [{:type :title
                           :content "OM - ClojureScript"
                           }]
               fixture-2 [{:type :title
                           :content "OM - ClojureScript"
                           }
                          {:type :list
                           :disclose true
                           :title "a list"
                           :content ["Clojure aber Zielplattform JavaScript", "ein Clojure Dialekt"]}]
               fixture-3 [{:type :title
                           :content "OM - ClojureScript"
                           }
                          {:type :list
                           :title "a list"
                           :content ["Clojure aber Zielplattform JavaScript", "ein Clojure Dialekt"]}]
               fixture-4 [{:type :title
                           :content "OM - ClojureScript"
                           }
                          {:type :list
                           :disclose true
                           :title "a list"
                           :content ["Clojure aber Zielplattform JavaScript", "ein Clojure Dialekt"]}
                          {:type :list
                           :disclose true
                           :title "a list"
                           :content ["Clojure aber Zielplattform JavaScript", "ein Clojure Dialekt"]}]]
              (testing "transform"
                       (is (= (count (pages/transform fixture-1)) 1))
                       (is (= (count (pages/transform fixture-2)) 3))
                       (is (= (count (pages/transform fixture-3)) 2))
                       (is (= (count (pages/transform fixture-4)) 5)))))
