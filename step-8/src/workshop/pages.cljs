(ns workshop.pages)

(def presentation

  [
   {:type :title
    :content "OM-ClojureScript"
    }

   {:type :section
    :content "OM"
    }

   {:type :list
    :title "Was ist OM?"
    :content ["Facebook", "Funktional", "Virtual-Dom-Tree"]
    }

   {:type :section
    :content "ClojureScript"
    }

   {:type :list
    :disclose true
    :title "Was ist ClojureScript?"
    :content ["Wie Clojure aber Zielplattform JavaScript", "ein Clojure Dialekt"]
   }
   ])

(defn transform [p]
  (reduce (fn [acc e]
            (if (:disclose e)
              (let [items (:content e)
                    item-count (count items)]
                (apply conj acc (map (fn [cnt]
                                       (assoc e :content (vec (take cnt items))
                                                :max-number item-count))
                                     (range 1 (inc item-count)))))
              (conj acc e))) [] p))



#_ (defn expand-all [pages]
  (letfn [(expand [page]
                  (let [items (:content page)
                        cnt (count items)]
                    (map (fn[i] (assoc page :content (take (inc i) items))) (range 0 cnt))))]
    (loop [page pages result []]
      (let [p (first page)
            r (rest page)]
        (if (seq p)
          (recur r
                 (if (:disclose p)
                   (apply conj result (expand p))
                   (conj result p)))
          result)))))

#_ (reduce + 0 [1 2 3 4])


#_ (reduce (fn [acc e]
      (conj acc e)) [] presentation)


#_ (let [e (last presentation)
      items (:content e)
      item-count (count items)]
    (map (fn [cnt]
         (assoc e :content (take (inc cnt) items)
                  :max-number item-count))
       (range 0 item-count)))

#_ (reduce (fn [acc e]
      (conj acc e)) [] presentation)

#_ (conj [1] [1 2 3])

#_ (apply conj [1] [1 2 3])

#_ (conj [1] 1 2 3)

(defn get-pages []
  (transform presentation))
