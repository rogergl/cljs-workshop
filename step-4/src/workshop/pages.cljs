(ns workshop.pages)

(def presentation

  [
   {:type    :title
    :content "OM - ClojureScript"
    }

   {:type    :section
    :content "OM"
    }

   {:type    :list
    :title   "Grundlagen"
    :content ["Facebook", "Funktional", "Virtual-Dom-Tree"]
    }

   {:type    :section
    :content "ClojureScript"
    }

   {:type     :list
    :disclose true
    :title    "a list"
    :content  ["Wie Clojure aber Zielplattform JavaScript", "ein Clojure Dialekt"]
    }
   ])

(defn get-pages []
  presentation)
