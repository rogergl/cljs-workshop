(ns workshop.pages)

(def presentation

  [
   {:type :title
    :content "OM - ClojureScript"
    }

   {:type :section
    :content "OM"
    }

   {:type :list
    :title "Grundlagen"
    :content ["Facebook", "Funktional", "Virtual-Dom-Tree"]
    }

   {:type :section
    :content "ClojureScript"
    }

   {:type :list
    :disclose true
    :title "a list"
    :content ["Wie Clojure aber Zielplattform JavaScript", "ein Clojure Dialekt"]
    }
   ])

(defn transform [p]
  (reduce (fn [acc e]
            (if (:disclose e)
              (let [items (:content e)
                    item-count (count items)]
                (apply conj acc (map (fn [cnt]
                                       (assoc e :content (take (inc cnt) items)
                                                :max-number item-count))
                                     (range 0 item-count))))
              (conj acc e))) [] p))

; explain steps

#_ (reduce (fn [acc e]
      (conj acc e)) [] presentation)


#_ (let [e (last presentation)
      items (:content e)
      item-count (count items)]
    (map (fn [cnt]
         (assoc e :content (take (inc cnt) items)
                  :max-number item-count))
       (range 0 item-count)))

#_ (reduce (fn [acc e]
      (conj acc e)) [] presentation)

#_ (conj [1] [1 2 3])

#_ (apply conj [1] [1 2 3])

#_ (conj [1] 1 2 3)

(defn get-pages []
  (transform presentation))
