(ns workshop.presenter
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))


(defn component [app owner]
  (reify om/IRender
    (render [_]
      (let [page (nth (:pages app) (:counter app))]
        (println page)
        (dom/h1 nil (:counter app))))))
