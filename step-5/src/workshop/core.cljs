(ns workshop.core
  "add transform to pages namespace"
  (:require [om.core :as om :include-macros true]
            [workshop.presenter :as presenter]
            [goog.events :as events]
            [om.dom :as dom :include-macros true]
            [workshop.pages :as pages])
  (:import
    [goog.events EventType]))

(enable-console-print!)

(def CURSOR-LEFT 37)

(def CURSOR-RIGHT 39)

(def app-state (atom {:pages  (pages/get-pages)
                      :counter 0}))

(om/root
  (fn [app owner]
    (reify

      om/IRender
      (render [_]
        (om/build presenter/component app))

      om/IDidMount
      (did-mount [_]
        (events/listen js/window EventType.KEYUP
                       (fn [e]
                         (let [kc (.-keyCode e)
                               cnt (:counter @app)
                               max (count (:pages @app))]
                           (cond
                             (== kc CURSOR-LEFT)
                             (when (> cnt 0)
                               (om/transact! app :counter dec))
                             (== kc CURSOR-RIGHT)
                             (when (< cnt (dec max))
                               (om/transact! app :counter inc))
                             :else ())))))
      ))
  app-state
  {:target (. js/document (getElementById "app"))})
