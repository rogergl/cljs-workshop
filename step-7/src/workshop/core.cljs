(ns workshop.core

  "adding core.async"

  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [om.core :as om :include-macros true]
            [workshop.presenter :as presenter]
            [workshop.pages :as pages]
            [goog.events :as events]
            [cljs.core.async :refer [>! <! put! chan]]
            [om.dom :as dom :include-macros true])
  (:import goog.History
           [goog.events EventType]))

(enable-console-print!)

(def app-state (atom {:pages (pages/get-pages)
                      :counter 0}))

;; inspired by a screencast about core.async from Cognitec
(defn events->chan
  ([el event-type c]
    (events/listen el event-type
                   (fn [e]
                     (put! c e)))
    c))


;; inspired by a screencast about core.async from Cognitect
(defn keys-chan
  []
  (events->chan js/window EventType.KEYUP
                (chan 1 (comp (map (fn [c] (.-keyCode c)))
                              (filter #{32 35 36 37 39})
                              (map {32 :next     ; space
                                    35 :end
                                    36 :home
                                    37 :previous ; left
                                    39 :next     ; right
                                    })))))

(om/root
  (fn [app owner]
    (reify

      om/IRender
      (render [_]
        (om/build presenter/component app))

      om/IDidMount
      (did-mount [_]
        (println app-state)
        (let [ch (keys-chan)]
          (go-loop [cmd (<! ch)]
                   (let [counter (:counter @app-state)
                         last-index (dec (count (:pages @app-state)))]
                     (case cmd
                       :previous (when (> counter 0) (om/transact! app :counter dec))
                       :next (when (< counter last-index) (om/transact! app :counter inc))
                       :home (om/update! app :counter 0)
                       :end (om/update! app :counter last-index)))
                   (recur (<! ch)))))
      ))
  app-state
  {:target (. js/document (getElementById "app"))})
